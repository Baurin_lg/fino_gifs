import requests
import json
import lxml
import os
from lxml import etree
from io import StringIO, BytesIO
import os

import sqlite3
import urllib
import os
from pathlib import Path
from PIL import Image
import sys
import uuid
import re
from moviepy.editor import VideoFileClip, concatenate_videoclips



class Scraper:

    def __init__(self):

        self.conn = self.create_connection()
        self.create_table()
        Path(FOLDER_TARGET).mkdir(parents=True, exist_ok=True)
        # self.start_loop()

    @staticmethod
    def make_request(page):

        if page == 1:
            url = BASE_URL
        else:
            url = BASE_URL + "page/" + str(page)

        response = requests.get(url)

        return response

    @staticmethod
    def make_parser(response):

        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(response.text), parser)

        return tree

    @staticmethod
    def get_videos(page, tree):

        videos = []
        for video in tree.findall(".//video"):
            # import pdb; pdb.set_trace()
            date_video_string = video.get("poster")
            finded_groups = re.search('[0-9]{4}\/[0-9]{2}', date_video_string)
            # import pdb; pdb.set_trace()
            if finded_groups:
                src_video = video.find("source").get("src")
                date_video = finded_groups.group(0)
                videos.append((date_video, src_video))
                # if src.find(".mp4") != -1:
                #     new_gif = img.get("src")
                #     if new_gif not in gifs:
                #         gifs.append(img.get("src"))

        # (date, video_src)
        return videos

    def make_video(self):

        """
        Make the video chop
        """

        clip1 = VideoFileClip("myvideo.mp4")
        clip2 = VideoFileClip("myvideo2.mp4")
        clip3 = VideoFileClip("myvideo3.mp4")
        final_clip = concatenate_videoclips([clip1,clip2,clip3])
        final_clip.write_videofile("my_concatenation.mp4")

        return


    def publish_video(self):

        """
        Make the video chop
        """

        return


    def start_loop(self):

        page = self.get_last_page()
        if not page: page = 1
        response = self.make_request(page)
        tree = self.make_parser(response)
        videos = []
        finish = False
        while not response.text.find(NO_MORE_PAGE_MESSAGE) != -1:
            while len(videos) < 15:
                for new_video in self.get_videos(page, tree):
                    videos.append(new_video)
                    print (len(videos))
                
                page += 1
                self.save_video(videos=videos, page=page)
                if len(videos) > 10:
                    self.make_video()
                    self.publish_video()
                    finish = True
                    break

                response = self.make_request(page)
                tree = self.make_parser(response)

            if finish:
                break 


    def save_video(self, videos, page):

        for video in videos:
            # if not self.check_video_exist(video):
            uuid_src = str(uuid.uuid1().int)
            file_name = self.get_file_name(video[1], uuid_src)
            file_target = FOLDER_TARGET + "/" + file_name
            urllib.request.urlretrieve(
                video[1], file_target
            )
            # im = Image.open(file_target)
            # width, height = im.size
            self.create_register_on_db(video, file_name, page, uuid_src)
            # sys.stdout.write('.')
            # sys.stdout.flush()

    @staticmethod
    def get_file_name(video_src, uuid_video):

        return uuid_video + "__" + video_src.split("/")[-1]

    def create_table(self):

        squery_create_table = """
            CREATE TABLE IF NOT EXISTS videos_finofilipino (
                id integer PRIMARY KEY,
                filename text NOT NULL,
                uuid text NOT NULL,
                date_video text NOT NULL,
                page integer NOT NULL,
                FOREIGN KEY (id) REFERENCES videos_finofilipino (id)
            );
        """
        cur = self.conn.cursor()
        cur.execute(squery_create_table)
        return

    def get_last_page(self):

        squery_get_gif = """
            SELECT max(page) FROM videos_finofilipino
        """
        cur = self.conn.cursor()
        result = cur.execute(squery_get_gif).fetchall()[0][0]
        return result

    def check_video_exist(self, video):

        squery_get_gif = """
            SELECT * FROM videos_finofilipino WHERE name = "%s" AND date_video = "%s"
        """ % (video[1], video[0])
        cur = self.conn.cursor()
        result = False if cur.execute(squery_get_gif).fetchall() == [] else True
        return result

    @staticmethod
    def create_connection():

        conn = None
        try:
            conn = sqlite3.connect("./db.sqlite3")
        except Exception as e:
            print(e)

        return conn

    def create_register_on_db(self, video, file_name, page, uuid_src):


        sql = """ INSERT INTO videos_finofilipino(filename, uuid, date_video, page)
                  VALUES("%s","%s","%s", %s) """ % (file_name, uuid_src, video[0], page)
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()

        return


if __name__ == "__main__":

    scraper_instance = Scraper()
    scraper_instance.start_loop()